import Foundation
import RxCocoa
import RxSwift

extension Reactive where Base: UIScrollView {

    private var edgeOffset: CGFloat {
        return 100
    }

    var isNearBottomEdge: Driver<Void> {
        let base = self.base
        return contentOffset
            .asDriver()
            .map { offset in
                return (base.contentOffset.y + base.frame.size.height + self.edgeOffset > base.contentSize.height) && offset != CGPoint.zero 
            }
            .distinctUntilChanged()
            .flatMap({ nearEdge in
                return nearEdge ? Driver.just(()) : Driver.never()
            })
    }

}
