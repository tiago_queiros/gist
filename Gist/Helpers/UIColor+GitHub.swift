//
//  UIColor+GitHub.swift
//  Gist
//
//  Created by Tiago Queirós on 23/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static var darkGray: UIColor {
        return colorWith255(34, green: 47, blue: 62, alpha: 1)
    }

    static var gray: UIColor {
        return colorWith255(87, green: 101, blue: 116, alpha: 1)
    }

    public static func colorWith255(_ red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: alpha)
    }
}
