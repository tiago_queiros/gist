//
//  UINavigationController+Style.swift
//  Gist
//
//  Created by Tiago Queirós on 24/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    func applyStyle() {
        navigationBar.barTintColor = .darkGray
        let textAttributes =  [NSAttributedString.Key.foregroundColor : UIColor.white]
        navigationBar.largeTitleTextAttributes = textAttributes
        navigationBar.titleTextAttributes = textAttributes
        navigationBar.tintColor = .white
    }
}
