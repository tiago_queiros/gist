import Foundation
import Cartography

public enum BorderPosition {
    case top
    case bottom
}

extension UIView {
    @discardableResult public func fillSuperview(
        horizontalMargin: CGFloat = 0,
        verticalMargin: CGFloat = 0,
        toSafeArea: Bool = false) -> ConstraintGroup?
    {
        guard let superview = superview else { return nil }
        return constrain(self, superview) { view, superview in
            if toSafeArea {
                view.top == superview.safeAreaIfAvailable.top + verticalMargin
                view.bottom == superview.safeAreaIfAvailable.bottom - verticalMargin
                view.left == superview.safeAreaIfAvailable.left + horizontalMargin
                view.right == superview.safeAreaIfAvailable.right - horizontalMargin
            } else {
                view.top == superview.top + verticalMargin
                view.bottom == superview.bottom - verticalMargin
                view.left == superview.left + horizontalMargin
                view.right == superview.right - horizontalMargin
            }
        }
    }
}
