import Cartography

extension ViewProxy {

    public var safeAreaIfAvailable: SupportsPositioningLayoutProxy {
        if #available(iOS 11.0, *) {
            return safeAreaLayoutGuide
        } else {
            return self
        }
    }
}
