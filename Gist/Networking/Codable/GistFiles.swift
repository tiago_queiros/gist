//
//  GistFiles.swift
//  Gist
//
//  Created by Tiago Queirós on 23/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation

struct GistFiles {
    let filename: String
    let language: String?
    let url: String
    
    enum CodingKeys: String, CodingKey {
        case url = "raw_url"
        case language
        case filename
    }
}

extension GistFiles: Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        url = try values.decode(String.self, forKey: .url)
        language = try? values.decode(String.self, forKey: .language)
        filename = try values.decode(String.self, forKey: .filename)
    }
}
