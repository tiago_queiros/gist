//
//  GistOwner.swift
//  Gist
//
//  Created by Tiago Queirós on 23/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation

struct GistOwner {
    let avatarUrl: String
    let name: String
    let id: Int
    let url: String
    
    enum CodingKeys: String, CodingKey {
        case avatarUrl = "avatar_url"
        case login
        case id
        case url = "html_url"
    }
}

extension GistOwner: Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        avatarUrl = try values.decode(String.self, forKey: .avatarUrl)
        name = try values.decode(String.self, forKey: .login)
        id = try values.decode(Int.self, forKey: .id)
        url = try values.decode(String.self, forKey: .url)
    }
}
