//
//  GistModel.swift
//  Gist
//
//  Created by Tiago Queirós on 23/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation

struct GistViewModel {
    let id: String
    let comments: Int
    let description: String?
    let owner: GistOwner
    let files: [String : GistFiles]
    
    var mainFileName: String? {
        return files.first?.key
    }
}

extension GistViewModel: Decodable {}
