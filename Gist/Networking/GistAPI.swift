//
//  GithubAPI.swift
//  Gist
//
//  Created by Tiago Queirós on 23/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import Moya

enum GistAPI {
    case `public`(page: Int)
    case gist(id: String)
}

extension GistAPI: TargetType {
    var path: String {
        let domain = "gists"
        switch self {
        case .public:
            return "\(domain)/public"
        case .gist(let id):
            return "\(domain)/\(id)"
        }
    }

    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .public(let page):
            return .requestParameters(parameters: ["page" : page], encoding: URLEncoding.queryString)
        case .gist(_):
            return .requestPlain
        }
    }

    var headers: [String : String]? {
        return nil
    }

    var baseURL: URL {
        return URL(string: GlobalConstants.gitHubAPI)!
    }

    var method: Moya.Method {
        return .get
    }
}
