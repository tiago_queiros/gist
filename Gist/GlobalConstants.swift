//
//  Const.swift
//  Gist
//
//  Created by Tiago Queirós on 23/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation

struct GlobalConstants {
    static let gitHubAPI = "https://api.github.com"
}
