//
//  FavoritesViewModel.swift
//  Gist
//
//  Created by Tiago Queirós on 25/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class FavoritesViewModel {
    let localGistRepository = LocalGistsRepository()
    
    func transform(input: Input) -> Output {
        let items = input.trigger.map {
            return self.localGistRepository.retrieve()
        }
        return Output(items: items.asDriver())
    }
}

extension FavoritesViewModel {
    struct Input {
        let trigger: Driver<Void>
    }
    
    struct Output {
        let items: Driver<[LocalGist]>
    }
}
