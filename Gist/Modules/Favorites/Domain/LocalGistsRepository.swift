//
//  LocalGistsRepository.swift
//  Gist
//
//  Created by Tiago Queirós on 25/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import RxSwift
import RxCocoa

class LocalGistsRepository {

    let realm: Realm

    init(realm: Realm = try! Realm()) {
        self.realm = realm
    }

    func retrieve() -> [LocalGist] {
        return Array(realm.objects(LocalGist.self))
    }
    
    func gist(withId id: String) -> LocalGist? {
        return realm.object(ofType: LocalGist.self, forPrimaryKey: id)
    }
    
    func isSaved(id: String) -> Bool {
       return gist(withId: id) != nil
    }
    
    func remove(id: String) {
        guard let gist = gist(withId: id) else { return }
        try! realm.write {
            realm.delete(gist)
        }
    }

    func add(id: String, fileName: String?, ownerName: String, ownerAvatar: String?) {
        let localGist = LocalGist()
        localGist.id = id
        localGist.fileName = fileName ?? ""
        localGist.ownerName = ownerName
        localGist.ownerAvatar = ownerAvatar ?? ""
        
        try! realm.write {
            realm.add(localGist)
        }
    }
}
