//
//  LocalGist.swift
//  Gist
//
//  Created by Tiago Queirós on 25/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import RealmSwift

class LocalGist: Object {
    @objc dynamic var id = ""
    @objc dynamic var fileName = ""
    @objc dynamic var ownerName = ""
    @objc dynamic var ownerAvatar = ""

    override static func primaryKey() -> String? {
        return "id"
    }
}
