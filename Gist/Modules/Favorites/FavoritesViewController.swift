//
//  FavoritesViewController.swift
//  Gist
//
//  Created by Tiago Queirós on 25/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class FavoritesViewController: UIViewController {

    struct Texts {
        static let favorites = "Favorites"
    }
    
    struct Constants {
        static let cellIdentifier = "GistCell"
    }

    let viewModel = FavoritesViewModel()
    let tableView = UITableView()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureLayout()
        bindViewModel()
    }

    func configureLayout() {
        navigationController?.applyStyle()
        navigationController?.navigationBar.prefersLargeTitles = true
        title = Texts.favorites

        view.addSubview(tableView)
        tableView.backgroundColor = UIColor.gray
        tableView.fillSuperview()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(GistListCell.self, forCellReuseIdentifier: Constants.cellIdentifier)
    }
    
    func bindViewModel() {
        let input = FavoritesViewModel.Input(trigger: rx.viewWillAppear)
        let output = viewModel.transform(input: input)
        
        output.items
            .drive(tableView.rx.items(cellIdentifier: Constants.cellIdentifier, cellType: GistListCell.self)) { _, model, cell in
                cell.setup(name: model.ownerName, fileName: model.fileName, avatarUrl: model.ownerAvatar)
            }
            .disposed(by: disposeBag)
        
        tableView.rx.modelSelected(LocalGist.self).subscribe(onNext: { [weak self] in
            let detailsVC = GistDetailsViewController(gistId: $0.id)
            self?.navigationController?.pushViewController(detailsVC, animated: true)
        }).disposed(by: disposeBag)
    }
}
