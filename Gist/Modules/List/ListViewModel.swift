//
//  ListViewModel.swift
//  Gist
//
//  Created by Tiago Queirós on 23/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class ListViewModel {
    private let gistRepository = GistRepository()
    private var currentPage = 0

    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        
        let items = Driver.merge([input.nextPageTrigger, input.trigger]).startWith(()).scan(0, accumulator: { pageNumber, _ in
            return pageNumber + 1
        }).flatMap {
            self.gistRepository.publicList(page: $0)
                .trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
            }.scan([]) {
                return $0 + $1
        }
        
        return Output(fetching: activityIndicator.asDriver(),
                      items: items.asDriver())
    }
}

extension ListViewModel {
    struct Input {
        let trigger: Driver<Void>
        let nextPageTrigger: Driver<Void>
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let items: Driver<[GistViewModel]>
    }
}
