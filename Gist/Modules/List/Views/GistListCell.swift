//
//  GistListCell.swift
//  Gist
//
//  Created by Tiago Queirós on 23/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import Cartography
import Kingfisher

class GistListCell: UITableViewCell {
    
    let avatarImageView = UIImageView()
    let nameLabel = UILabel()
    let fileLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configurelayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configurelayout() {
        contentView.addSubview(avatarImageView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(fileLabel)

        backgroundColor = UIColor.gray
        
        nameLabel.textColor = .white
        nameLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        nameLabel.numberOfLines = 0

        fileLabel.textColor = .white
        fileLabel.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        fileLabel.numberOfLines = 0

        avatarImageView.layer.cornerRadius = 4
        avatarImageView.clipsToBounds = true

        constrain(avatarImageView, nameLabel, fileLabel, contentView) { avatar, name, file, superview in
            avatar.leading == superview.leading + 16
            avatar.height == 30
            avatar.width == 30
            avatar.top == superview.top + 16

            name.leading == avatar.trailing + 16
            name.top == superview.top + 16
            name.trailing == superview.trailing - 16
            
            file.top == name.bottom + 1
            file.trailing == superview.trailing - 16
            file.leading == avatar.trailing + 16
            file.bottom == superview.bottom - 16
        }
        
        selectionStyle = .none
        accessoryType = .disclosureIndicator
    }
    
    func setup(name: String, fileName: String?, avatarUrl: String?) {
        nameLabel.text = name
        fileLabel.text = fileName
        if let avatarUrl = avatarUrl,
            let url = URL(string: avatarUrl) {
            avatarImageView.kf.setImage(with: url)
        } else {
            avatarImageView.image = nil
        }
    }
}
