//
//  ListViewController.swift
//  Gist
//
//  Created by Tiago Queirós on 23/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import UIKit
import Cartography
import RxSwift
import RxCocoa

class ListTableViewController: UIViewController {

    struct Texts {
        static let gists = "Gists"
    }
    
    struct Constants {
        static let cellIdentifier = "GistCell"
    }
    
    let tableView = UITableView()
    let viewModel = ListViewModel()
    let disposeBag = DisposeBag()
    let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureLayout()
        bindViewModel()
    }
    
    func configureLayout() {
        navigationController?.applyStyle()
        view.addSubview(tableView)
        tableView.backgroundColor = UIColor.gray
        tableView.fillSuperview()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(GistListCell.self, forCellReuseIdentifier: Constants.cellIdentifier)
        tableView.separatorColor = UIColor.white.withAlphaComponent(0.4)
        tableView.addSubview(refreshControl)
        refreshControl.tintColor = .white

        navigationController?.navigationBar.prefersLargeTitles = true
        title = Texts.gists
    }
    
    func bindViewModel() {
        let trigger = Driver.merge([rx.firstTimeAppearing])
        let input = ListViewModel.Input(trigger: trigger, nextPageTrigger: tableView.rx.isNearBottomEdge)
        let output = viewModel.transform(input: input)
        
        output.items
             .drive(tableView.rx.items(cellIdentifier: Constants.cellIdentifier, cellType: GistListCell.self)) { _, model, cell in
                cell.setup(name: model.owner.name, fileName: model.mainFileName, avatarUrl: model.owner.avatarUrl)
            }
            .disposed(by: disposeBag)

        tableView.rx.modelSelected(GistViewModel.self).subscribe(onNext: { [weak self] in
            let detailsVC = GistDetailsViewController(gistModel: $0)
            self?.navigationController?.pushViewController(detailsVC, animated: true)
        }).disposed(by: disposeBag)

        output.fetching
            .drive(refreshControl.rx.isRefreshing)
            .disposed(by: disposeBag)

    }
}
