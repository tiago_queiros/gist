//
//  GistRepository.swift
//  Gist
//
//  Created by Tiago Queirós on 23/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya

class GistRepository {
    let provider = MoyaProvider<GistAPI>()

    func publicList(page: Int) -> Observable<[GistViewModel]> {
        return provider.rx.request(.public(page: page))
            .map([GistViewModel].self)
            .asObservable()
    }
    
    func gist(id: String) -> Observable<GistViewModel> {
        return provider.rx.request(.gist(id: id))
            .map(GistViewModel.self)
            .asObservable()
    }
}
