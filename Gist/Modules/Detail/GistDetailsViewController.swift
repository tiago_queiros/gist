//
//  GistDetailViewController.swift
//  Gist
//
//  Created by Tiago Queirós on 24/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class GistDetailsViewController: UITableViewController {

    struct Texts {
        static let details = "Details"
        static let favorite = "Favorite"
        static let unfavorite = "Unfavorite"
    }
    
    struct Constants {
        static let descriptionCellIdentifier = "DescriptionCellIdentifier"
        static let actionCellIdentifier = "ActionCellIdentifier"
    }
    
    let viewModel: GistDetailsViewModel
    let disposeBag = DisposeBag()
    var sections = [GistDetailSection]()
    let favoriteBarItem = UIBarButtonItem()

    init(gistId: String) {
        self.viewModel = GistDetailsViewModel(gistId: gistId)
        super.init(nibName: nil, bundle: nil)
    }
    
    init(gistModel: GistViewModel) {
        self.viewModel = GistDetailsViewModel(gistModel: gistModel)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLayout()
        bindViewModel()
    }
    
    func configureLayout() {
        tableView.backgroundColor = UIColor.gray
        tableView.fillSuperview()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorColor = UIColor.white.withAlphaComponent(0.4)
        navigationController?.navigationBar.prefersLargeTitles = true
        title = Texts.details
        navigationItem.rightBarButtonItem = favoriteBarItem
        
        GistDetailSection.CellType.allCases.forEach {
            switch $0 {
            case .description(_, _):
                self.tableView.register(DescriptionCell.self, forCellReuseIdentifier: Constants.descriptionCellIdentifier)
            case .action(_, _):
                self.tableView.register(ActionCell.self, forCellReuseIdentifier: Constants.actionCellIdentifier)
            }
        }
    }

    func bindViewModel() {
        let trigger = Driver.merge(rx.firstTimeAppearing)
        let input = GistDetailsViewModel.Input(trigger: trigger, favoriteTrigger: favoriteBarItem.rx.tap.asDriver())
        let output = viewModel.transform(input: input)
        
        output.items
            .drive(onNext: { [weak self] in
                self?.sections = $0
                self?.tableView.reloadData()
            }).disposed(by: disposeBag)
        
        output.favoriteStatus.drive(onNext: { [weak self] in
            let title = $0 ? Texts.unfavorite : Texts.favorite
            self?.favoriteBarItem.title = title
        }).disposed(by: disposeBag)
    }
}

extension GistDetailsViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].content.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = sections[indexPath.section].content[indexPath.row]
        switch item {
        case .description(let title, let description):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.descriptionCellIdentifier, for: indexPath) as? DescriptionCell else { fatalError() }
            cell.setup(title: title, description: description)
            return cell
        case .action(let title, _):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.actionCellIdentifier, for: indexPath) as? ActionCell else { fatalError() }
            cell.setup(with: title)
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = sections[indexPath.section].content[indexPath.row]
        switch item {
        case .action(_, let url):
            guard let url = URL(string: url) else { return }
            presentWebView(for: url)
        default:
            return
        }
    }
    
    func presentWebView(for url: URL) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
