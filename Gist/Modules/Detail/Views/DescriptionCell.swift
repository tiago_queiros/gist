//
//  DescriptionCell.swift
//  Gist
//
//  Created by Tiago Queirós on 24/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import UIKit
import Cartography

class DescriptionCell: UITableViewCell {
    
    let titleLabel = UILabel()
    let descriptionLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureLayout() {
        backgroundColor = .gray
        selectionStyle = .none
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        titleLabel.textColor = .white
        titleLabel.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)

        descriptionLabel.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        descriptionLabel.textColor = UIColor.white.withAlphaComponent(0.8)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)

        constrain(titleLabel, descriptionLabel, contentView) { title, description, superview in
            title.leading == superview.leading + 16
            title.centerY == superview.centerYWithinMargins

            description.leading >= title.trailing + 10
            description.top == superview.top + 16
            description.trailing == superview.trailing - 16
            description.bottom == superview.bottom - 10
        }
    }
    
    func setup(title: String, description: String) {
        titleLabel.text = title
        descriptionLabel.text = description
    }
}
