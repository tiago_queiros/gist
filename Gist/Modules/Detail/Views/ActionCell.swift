//
//  ActionCell.swift
//  Gist
//
//  Created by Tiago Queirós on 25/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import UIKit
import Cartography

class ActionCell: UITableViewCell {
    let titleLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureLayout() {
        backgroundColor = .gray
        accessoryType = .disclosureIndicator
        selectionStyle = .none

        contentView.addSubview(titleLabel)
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        titleLabel.textColor = .white
        titleLabel.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        titleLabel.numberOfLines = 0

        constrain(titleLabel, contentView) { title, superview in
            title.leading == superview.leading + 16
            title.top == superview.top + 10
            title.bottom == superview.bottom - 10
            title.trailing == superview.trailing - 10
        }
    }
    
    func setup(with title: String) {
        titleLabel.text = title
    }
}
