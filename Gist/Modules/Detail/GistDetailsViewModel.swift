//
//  GistDetailsViewModel.swift
//  Gist
//
//  Created by Tiago Queirós on 24/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class GistDetailsViewModel {
    struct Texts {
        static let name = "Name"
        static let details = "Details"
        static let comments = "Comments"
        static let description = "Description"
        static let user = "User"
        static let viewProfile = "View Profile"
        static let files = "Files"
    }
    
    private var gistModel: GistViewModel?
    private let gistId: String

    private let localGistRepository = LocalGistsRepository()
    private let gistRepository = GistRepository()

    var gistIsSaved: Bool {
        return localGistRepository.isSaved(id: gistId)
    }

    init(gistModel: GistViewModel) {
        self.gistModel = gistModel
        self.gistId = gistModel.id
    }

    init(gistId: String) {
        self.gistId = gistId
    }
    
    func transform(input: Input) -> Output {
        let cells = input.trigger
            .asObservable()
            .flatMap { _ in
                return self.retrieveGistIfNeeded()
            }.map { _ in
                return self.buildCells()
            }.asDriverOnErrorJustComplete()

        let initialFavoriteStatus = input.trigger.map {
            self.gistIsSaved
            }.asDriver()
        
        let favoriteStatus = input.favoriteTrigger.map {
            return self.saveOrRemoveGist()
        }.asDriver()
        
        return Output(items: cells.asDriver(),
                      favoriteStatus: Driver.merge([favoriteStatus, initialFavoriteStatus]))
    }
    
    private func saveOrRemoveGist() -> Bool {
        guard let gistModel = gistModel else { return false}
        let currentStatus = gistIsSaved
        if currentStatus {
            localGistRepository.remove(id: gistModel.id)
        } else {
            localGistRepository.add(id: gistModel.id, fileName: gistModel.mainFileName, ownerName: gistModel.owner.name, ownerAvatar: gistModel.owner.avatarUrl)
        }
        return !currentStatus
    }
    
    private func buildCells() -> [GistDetailSection] {
        guard let gistModel = gistModel else { return [] }
        let userSection = GistDetailSection(title: Texts.user, content: [
            .description(title: "id", description: "\(gistModel.owner.id)"),
            .description(title: Texts.name, description: gistModel.owner.name),
            .action(title: Texts.viewProfile, url: gistModel.owner.url)
            ])

        let detailsSection = GistDetailSection(title: Texts.details, content: [
            .description(title: "id", description: gistModel.id),
            .description(title: Texts.comments, description:"\(gistModel.comments)"),
            .description(title: Texts.description, description: gistModel.description ?? "-"),
            ])
        
        let files = gistModel.files.values.map { file -> GistDetailSection.CellType in
            return .action(title: file.filename, url: file.url)
        }
        let filesSection = GistDetailSection(title: Texts.files, content: files)

        return [userSection, detailsSection, filesSection]
    }

    private func retrieveGistIfNeeded() -> Observable<GistViewModel> {
        if let gistModel = gistModel {
            return Observable.just(gistModel)
        }
        return gistRepository.gist(id: gistId).do(onNext: { [weak self] in
            self?.gistModel = $0
        })
    }
}

extension GistDetailsViewModel {
    struct Input {
        let trigger: Driver<Void>
        let favoriteTrigger: Driver<Void>
    }
    
    struct Output {
        let items: Driver<[GistDetailSection]>
        let favoriteStatus: Driver<Bool>
    }
}

