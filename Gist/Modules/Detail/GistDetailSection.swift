//
//  GistDetailSection.swift
//  Gist
//
//  Created by Tiago Queirós on 24/09/2018.
//  Copyright © 2018 Tiago Queiros. All rights reserved.
//

import Foundation

struct GistDetailSection {
    let title: String
    let content: [CellType]
}

extension GistDetailSection {
    enum CellType: CaseIterable {
        static var allCases: [CellType] {
            return [.description(title: "", description: ""), .action(title: "", url: "")]
        }

        case description(title: String, description: String)
        case action(title: String, url: String)
    }

}
